from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import Required, Length, Email, Regexp, EqualTo
from wtforms import ValidationError
from wtforms.widgets import TextArea
from ..models import User


class NameForm(Form):
    name = StringField('What is your name?', validators=[Required()])
    submit = SubmitField('Submit')


class NewRequestForm(Form):
    title = StringField('Title', validators=[Required(), Length(1,64)])
    description = StringField('Description', validators=[Required()],widget=TextArea())
    dataset = StringField('Dataset', validators=[Required()],widget=TextArea())
    objectives = StringField('Objectives', validators=[Required()],widget=TextArea())
    submit = SubmitField('Send Request')

class NewMessage(Form):
	text = StringField('Message', validators=[Required(), Length(1,2048)],widget=TextArea())
	submit = SubmitField('Send')