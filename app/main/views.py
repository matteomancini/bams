from flask import render_template, flash,redirect,url_for
from . import main
from flask.ext.login import login_user, logout_user, login_required, \
    current_user
from .forms import NewRequestForm, NewMessage
from ..models import Project,State,User,Message
from .. import db
from ..email import send_email


@main.route('/')
def index():
	return redirect(url_for('auth.login'))

@main.route('/home_admin')
@login_required
def home_admin():
	if current_user.is_administrator:
		projects=Project.query.all()
		count=sum(p.state_id == 1 for p in projects)
		return render_template('home_admin.html',count=count)
	return render_template('home_client.html')

@main.route('/home_client')
@login_required
def home_client():
	if current_user.is_administrator:
		projectsss=Project.query.all()
		count=sum(pr.state_id == 1 for pr in projectsss)
		return render_template('home_admin.html',count=count)
	return render_template('home_client.html')

@main.route('/new_request',methods=['GET', 'POST'])
@login_required
def new_request():
	if current_user.is_administrator:
			projects=Project.query.all()
			count=sum(p.state_id == 1 for p in projects)
			return render_template('home_admin.html',count=count)
	form = NewRequestForm()
	if form.validate_on_submit():
		project =Project(title=form.title.data,description=form.description.data,objectives=form.objectives.data,dataset=form.dataset.data,owner_id=current_user.id)
		db.session.add(project)
		#devo recuperare la mail dell'unico admin e inviare la conferma della creazione del nuovo progetto
		admin = User.query.filter_by(is_administrator=True).first()
		if admin is not None:
			send_email(admin.email, 'New Project on Bams',
				'mail/new_project', admin=admin, user=current_user, project=project)
			flash('Request successfully sent. A message to notify the new project has been sent')
		else:
			flash('Admin not found')

                    
		return render_template('home_client.html')
	return render_template('new_request.html',form=form)

@main.route('/client_projects',defaults={'sort':'Newest','id_state':None,'id_project':None},methods=['GET','POST'])
@main.route('/client_projects/<sort>',defaults={'id_state':None,'id_project':None},methods=['GET','POST'])
@main.route('/client_projects/<id_state>/<id_project>',defaults={'sort':'Newest'},methods=['GET','POST'])
@main.route('/client_projects/<id_state>/<id_project>/<sort>',methods=['GET','POST'])

@login_required
def client_projects(sort,id_state,id_project):
	if current_user.is_administrator:
		projects=Project.query.all()
		count=sum(p.state_id == 1 for p in projects)
		return render_template('home_admin.html',count=count)
	projects = current_user.owned
	if id_state is not None and id_project is not None:
		project = Project.query.get(id_project)
		project.view_client=0
		db.session.commit()
		flash('project removed from list')

	
	map = {}
	messages = current_user.received
	for p in projects:
		count=sum( mess.read== 0 and p.id == mess.project_id for mess in messages)
		map[p.id] = count

	if sort is not None:

		if sort=='A to Z':
			projects = projects.order_by(Project.title)
		elif sort=='Newest':
			projects = projects.order_by(Project.created_at.desc())
		elif sort=='Oldest':
			projects = projects.order_by(Project.created_at)
		else :
			sort='Newest'
			projects = projects.order_by(Project.created_at.desc())
	
	return render_template('client_projects.html',projects=projects,ord=sort, map=map)

@main.route('/admin_projects/<id_state>/<id_project>/<sort>',methods=['GET','POST'])
@main.route('/admin_projects',defaults={'id_state': None, 'id_project': None,'sort':'Newest'},methods=['GET','POST'])
@main.route('/admin_projects/<id_state>/<id_project>',defaults={'sort':'Newest'},methods=['GET','POST'])
@main.route('/admin_projects/<sort>',defaults={'id_state':None,'id_project':None},methods=['GET','POST'])
@login_required
def admin_projects(id_state,id_project,sort):
	if not current_user.is_administrator:
		return render_template('home_client.html')
	if id_state is not None and id_project is not None:
		project= Project.query.get(id_project)
		if project is None:
			projectsss=Project.query.all()
			count=sum(pr.state_id == 1 for pr in projectsss)
			flash('project not found')
			return render_template('home_admin.html',count=count)
		else:
			if id_state != '1' and id_state != '2' and id_state != '3' and id_state != '4' and id_state != '5' and id_state != '6' and id_state != '7' and id_state != '8' and id_state != '9' and id_state != '10' and id_state != '11' and id_state != '12':
				flash('state not allowed')
				projectsss=Project.query.all()
				count=sum(pr.state_id == 1 for pr in projectsss)
				return render_template('home_admin.html',count=count)
			if id_state=='9':
				project.chat_enabled=1
				db.session.commit()
			if id_state=='2' or id_state=='11':
				project.chat_enabled=0
				db.session.commit()
			if id_state=='delete':
				project.view_admin=0
				db.session.commit()
				flash('project destroyed')

			else:
				user=project.owner
				old_state=project.state.name
				new_state=State.query.get(id_state).name
				project.state_id=id_state
				db.session.commit()
				send_email(user.email,'State Changed',
				'/mail/state_changed', admin=current_user , user=user,project=project,old_state=old_state,new_state=new_state)
				flash('A message to notify the changed state has been sent')

	map = {}
	messages = current_user.received
	projects = Project.query.all()
	for p in projects:
		count=sum( mess.read == 0 and p.id == mess.project_id for mess in messages)
		map[p.id] = count
	print(map)

	states= State.query.all()
	if sort is not None:

		if sort=='A to Z':
			projects = Project.query.order_by(Project.title)
		elif sort=='Newest':
			projects = Project.query.order_by(Project.created_at.desc())
		elif sort=='Oldest':
			projects = Project.query.order_by(Project.created_at)
		else :
			print(sort)
			sort = 'Newest'
			projects = Project.query.order_by(Project.created_at.desc())

	
	return render_template('admin_projects.html',projects=projects, states=states, map=map ,ord=sort)

@main.route('/project_description/<id>',methods=['GET','POST'])
@login_required
def project_description(id):
	if not current_user.is_administrator:
		project = Project.query.get(id)
		if project not in current_user.owned:
			flash('Permission denied')
			return render_template('home_client.html')
	project = Project.query.get(id)
	if project is None:
		flash('project not found')
		projectsss=Project.query.all()
		count=sum(pr.state_id == 1 for pr in projectsss)
		return render_template('home_admin.html',count=count)
	else:
		return render_template('project_description.html',project=project)

@main.route('/chat/<id>',defaults={'id_owner':None},methods=['GET','POST'])
@main.route('/chat/<id_owner>/<id>', methods=['GET', 'POST'])
@login_required
def chat(id_owner, id):
	form = NewMessage()
	messages = Message.query.all()
	proj = Project.query.get(id)
	mess_received = current_user.received

	for m in mess_received:
		if proj is not None:
			if m.project_id == proj.id:
				m.read = 1
				db.session.commit()

	if current_user.is_administrator:
		if proj is None or id_owner is None or proj.state_id==11 or proj.owner_id!=int(id_owner):
			flash('chat not found')
			projectsss=Project.query.all()
			count=sum(pr.state_id == 1 for pr in projectsss)
			return render_template('home_admin.html',count=count)


		if form.validate_on_submit():
			message = Message(text = form.text.data, from_id = current_user.id, to_id = id_owner, project_id = id)
			db.session.add(message)
			project = Project.query.get(id)
			project.chat_enabled = 1
			db.session.commit()
			user = User.query.get(id_owner)
			send_email(user.email,'New Message', '/mail/message', to_user = user, from_user = current_user, message = message.text)
			return redirect(url_for('main.chat', id = id, id_owner = id_owner)) 
	else:
		if  proj not in current_user.owned or not proj.chat_enabled :
			flash('Permission denied')
			return render_template('home_client.html')


		admin = User.query.filter_by(is_administrator=True).first()
		if form.validate_on_submit():
			message = Message(text = form.text.data, from_id = current_user.id, to_id = admin.id, project_id = id)
			db.session.add(message)
			send_email(admin.email,'New Message', '/mail/message', to_user = admin, from_user = current_user)
			return redirect(url_for('main.chat', id = id)) 
	return render_template('chat.html',form = form, messages = messages, from_id = current_user, project = proj)


