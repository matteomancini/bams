from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from flask.ext.login import UserMixin,AnonymousUserMixin
from . import db, login_manager
import datetime


def _get_date():
    return datetime.datetime.now()


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    is_administrator = db.Column(db.Boolean, default=False)
    confirmed = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.Date(), default=_get_date)
    updated_at = db.Column(db.Date(), onupdate=_get_date)
    sent = db.relationship("Message", backref="sender", lazy = "dynamic",foreign_keys = 'Message.from_id')
    received = db.relationship("Message", backref="receiver", lazy = "dynamic" ,foreign_keys = 'Message.to_id')
    owned = db.relationship("Project", backref="owner", lazy = "dynamic" ,foreign_keys = 'Project.owner_id')

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.id})

    def confirm(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        return True

    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id})

    def reset_password(self, token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('reset') != self.id:
            return False
        self.password = new_password
        db.session.add(self)
        return True

    def generate_email_change_token(self, new_email, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'change_email': self.id, 'new_email': new_email})

    def change_email(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('change_email') != self.id:
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        db.session.add(self)
        return True
    
    def is_admin(self):
        return self.is_administrator


    def __repr__(self):
        return '<User %r>' % self.username


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

'''REALIZZAZIONE DEL DB''' 

'''Table-Project '''

class Project(db.Model):
    __tablename__='project'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50),nullable=False)
    description = db.Column(db.Text(),nullable=False)
    objectives = db.Column(db.Text(),nullable=True)
    dataset = db.Column(db.Text(),nullable=True)
    view_admin = db.Column(db.Boolean, default=True)
    view_client = db.Column(db.Boolean, default=True)
    created_at = db.Column(db.Date(), default=_get_date)
    updated_at = db.Column(db.Date(), onupdate=_get_date)
    chat_enabled = db.Column(db.Boolean, default=False)


    '''id dello stato a cui si riferisce'''
    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), default=1);

    '''relazione tra project e message '''
    conversation = db.relationship('Message', backref='project',lazy='dynamic',foreign_keys = 'Message.project_id')

    '''chiave esterna, creatore del progetto'''
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'))


'''Table-State  contiene al suo interno lo stato del progetto e il colore associato ad esso '''

class State(db.Model):
    __tablename__='state'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50),nullable=False)
    color = db.Column(db.String(50),nullable=False)
    created_at = db.Column(db.Date(), default=_get_date)
    updated_at = db.Column(db.Date(), onupdate=_get_date)
    projects = db.relationship('Project', backref='state',lazy='dynamic',foreign_keys = 'Project.state_id')

'''Table-Message '''

class Message(db.Model):
    __tablename__='message'
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(250), nullable=False)
    from_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    to_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    read = db.Column(db.Boolean(),default=False)
    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    created_at = db.Column(db.Date(), default=_get_date)
    updated_at = db.Column(db.Date(), onupdate=_get_date)


    
