# Bams
Questa vuole essere una linea guida per l'utilizzo del progetto e un riferimento per la fase di start-up dello stesso.

# Start-up
What you need:
## FLASK:
### We recommend to install virtualenv, cause multiple projects in python means that you need multiple versions of Python, or multiple versions of Python libraries ( on Mac OSX: $ sudo easy_install virtualenv or $ sudo pip install virtualenv )
###Now you've to create the new folder where Bams will run: 
#### $ mkdir bams
#### $ cd bams
#### $ virtualenv flask 
### Now, whenever you want to work on a project, you only have to activate the corresponding environment:
#### On OS X: $ . venv/bin/activate
#### If you are a Windows user, the following command is for you: $ venv\scripts\activate
### Now, after the virtual env has been activated,  you can just enter the following command to get Flask activated in your virtualenv:
#### $ pip install Flask
###source: http://flask.pocoo.org/docs/0.10/installation/
##BAMS:
### Now in your bams folder you'll have a flask folder
### Paste in bams the project hosted here on Bitbucket

## SETUP AND CONFIG:
### 1) in your db-server create 3 empty database bams, bamsdev, bamstest (We recommend to use MySQL, if u want to switch to another db you'll have to change the db-connectors in config.py)
### 2) open the config.py file and replace all the strings that have the prefix YOUR_ with the required informations. (eg. YOUR_GMAIL_ACCOUNT with mygmailusername@gmail.com )
#### in terminal (prompt for windows users) cd in bams folder and type:
##### flask/bin/pip install -r requirements.txt   (Flask modules and Python libraries will be installed in our folder after that)
##### flask/bin/python manage.py db init (after that in our folder will be a migrations subfolder)
##### flask/bin/python manage.py db migrate 
##### flask/bin/python manage.py db upgrade (this will fill up our remote db with schemas)
##### flask/bin/python manage.py runserver (this will start the server) NB. with our configurations it will be at localhost:5000
### close terminal and go in your db aministration tool (like phpMyAdmin or MySQL workbench) and fill up the states table with those state:
##### id | name 
##### 1		Waiting for Confirmation
##### 2		Confirmed
##### 3		Waiting for Data
##### 4		Analysis Scheduled at DATE
##### 5		Analysis Posponed at DATE
##### 6		Running
##### 7		Brainstorming
##### 8		Feedback request
##### 9		Finished
##### 10	Refuse
##### 11	Aborted
### now open your browser and in the address type localhost:5000
#### register the admin 
#### in your db aministration tool change the role of the user just registerd in the users table from 'client' to 'admin'

