import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'igitusfigitus'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = 'YOUR_GMAIL_ACCOUNT'
    MAIL_PASSWORD =  'YOUR_GMAIL_ACCOUNT_PASSWORD'
    FLASKY_MAIL_SUBJECT_PREFIX = '[BAMS]'
    FLASKY_MAIL_SENDER = 'BAMS - Bioinformatics Analysis Management System <bams@noreply.com>'
    FLASKY_ADMIN = os.environ.get('FLASKY_ADMIN')

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'mysql+pymysql://YOUR_USERNAME_MYSQL:YOUR_PASSWORD_MYSQL@127.0.0.1:3307/bamsdev'


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
        'mysql+pymysql://YOUR_USERNAME_MYSQL:YOUR_PASSWORD_MYSQL@127.0.0.1:3307/bamstest'


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'mysql+pymysql://YOUR_USERNAME_MYSQL:YOUR_PASSWORD_MYSQL@127.0.0.1:3307/bams'


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,

    'default': DevelopmentConfig
}